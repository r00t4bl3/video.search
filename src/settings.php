<?php
return [
    'settings' => [
        'siteName'          => 'QYou Videos',
        'baseUrl'           => 'http://localhost:8000/',
        'sparkpost_api_key' => '814f36743b960e56a82b7ddc8244f7452eb4cf8e',
        'receiver_name'     => 'Andrew',
        'receiver_email'    => 'akamphey@qyoutv.com',
        'sparkpost_api_key' => '814f36743b960e56a82b7ddc8244f7452eb4cf8e',
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Algolia API key
        'algolia' => [
            'application_id'            => 'H8E6MXIT0F',
            'search_only_api_key'       => 'b5aefb6df65dd57031e21f1b3510c1f4',
            'index_name'                => 'latamrepresent',
        ],

        // Twig settings
        'view' => [
            'template_path' => __DIR__ . '/../templates/',
            'cache_path' => __DIR__ . '/../templates/cache/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

    ],
];
