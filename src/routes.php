<?php

namespace App;

// Libraries
use GuzzleHttp\Client;
use SparkPost\SparkPost;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;


// Routes

$app->get('/', function ($req, $res, $args) {
    // Sample log message
    // $this->logger->info("Slim-Skeleton '/' route");

    $message = (isset( $this->flash->getMessages()['message'] )) ? $this->flash->getMessages()['message'][0] : null;

    return $this->view->render($res, 'alternate.phtml', [
        'message'   => $message,
        'algolia'   => $this->get('settings')['algolia'],
        'sitename'  => $this->get('settings')['siteName'],
        'baseurl'   => $this->get('settings')['baseUrl'],
    ]);
});



$app->get('/old', function ($req, $res, $args) {
    // Sample log message
    // $this->logger->info("Slim-Skeleton '/' route");

    $message = (isset( $this->flash->getMessages()['message'] )) ? $this->flash->getMessages()['message'][0] : null;

    return $this->view->render($res, 'index.phtml', [
        'message'   => $message,
        'algolia'   => $this->get('settings')['algolia'],
        'sitename'  => $this->get('settings')['siteName'],
        'baseurl'   => $this->get('settings')['baseUrl'],
    ]);
});



$app->post('/mail', function ($req, $res, $args) {

    $settings        = $this->get('settings');
    $sparkpost_api_key  = $settings['sparkpost_api_key'];

    $parsedBody     = $req->getParsedBody();
    $video_ids      = array_unique($parsedBody['video_ids']);

    foreach ($video_ids as &$video_id)
    {
        $video_id   = filter_var($video_id, FILTER_SANITIZE_STRING);
    }

    $mail_body = $this->view->fetch('email-inlined.phtml', [
        'name'      => $settings['receiver_name'],
        'video_ids' => $video_ids,
    ]);

    $this->flash->addMessage('message', 'Email sent successfully');

    // /*

    // Create transport
    $transport = (new \Swift_SmtpTransport('smtp.sparkpostmail.com', 587, 'tls'))
        ->setUsername('SMTP_Injection')
        ->setPassword($sparkpost_api_key);

    // Create the Mailer using your created Transport
    $mailer = new \Swift_Mailer($transport);

    // Create a message
    $message = (new \Swift_Message('Video Selection'))
        ->setFrom(['noreply@qyoutv.com' => 'QYou Videos'])
        ->setTo([$settings['receiver_email'] => $settings['receiver_name']])
        ->setBody('This is a HTML message.')
        ->addPart($mail_body, 'text/html');

    // Send the message
    $result = $mailer->send($message);

    if ($result)
    {
        // Set flash message for next request
        $this->flash->addMessage('message', 'Email sent successfully');
        // echo "asdf";
    }

    // */
    // Send to homepage
    return $res->withRedirect('/');


});
